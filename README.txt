Author:     Elisabete Baker

Date:       April 2017

Project:    Elisabete’s Work Samples

----

Hello there! Welcome to my README file. 

The “EB_Samples_2017” directory, contains programs and 
tutorials I have written over the years, separated into 
four categories:

* Front End (Web Development)
* Back End (SQL Database)
* Data Analysis (R)
* Tutorials (Comics, Blog posts, “How-To” guides, etc.)

Everything is in PDF format, with the exception of the
Front End, where I’ve included the various files used to 
create my former [FooBarBetty website](https://web.archive.org/web/20150511195548/foobarbetty.com).

I hope this helps you gain an understanding of my coding 
style, skillset and thought process.